# Homework 5

### Due Wed 2/24/21 @ 12a


In the next two homeworks, we will be re-tracing some of Megi's steps: using his source code to
re-build a boot image from scratch by ourselves. But we will do a few things differently:

-	We will only provide the Lune-OS operating system inside the P-Boot menu
-	We will use the latest release of Lune-OS
-	We will use the latest Linux Kernel

For building (cross-compiling) all this software, we will require a very particular "build 
environment." A build environment includes compiler, libraries of source code, ABIs (application binary interfaces -- in our case the ARM EABI, or Embedded binary interface), and lots of additional tools to help automate the build process, etc. A lot of these are contingent upon the hardware Linux thinks its running on.

This may pose a challenge for those of us who are running Ubuntu inside Windows using WSL/WSL2.
And so at this point, I recommend setting up an Xubuntu virtual machine using VirtualBox (https://www.youtube.com/playlist?list=PLrJLEXbQpxazlUbpukxgk6U7ufzTGKr1x).
This will guarantee uniformity for all of us, as all of our "hardware platforms" (from the vantage of the linux system) will be precisely
known and will be identical.

While creating your new virtual machine, choose a "Dynamically Allocated Hard Disk," but set the initial size to 100GB. During the build, we will have to hold large quantities of source code and
intermediate build products. This will require almost 100GB of free space on your system.
If you do not have 100GB free: give as much as you can; if your LuneOS build crashes (the last step), just include a screenshot of the crash.

After creating your virtual instance, you should modify the system settings:

- Increase number of CPUs to about 3/4 of the number of cores on your host (your physical computer).
- Increase the main memory (RAM) to 4GB
- Increase the video RAM (VRAM) to 64GB
- enable bidirectional shared clipboard (video)
- Add a shared folder with the host (video)
- Add your user to the "vboxsf" group: `sudo adduser "$(id -u)" vboxsf`
- Install VirtualBox Linux Guest Additions (video)

Now let's begin.



#### First we get local copies of everything we need:

We are going to need:

1. the P-boot source code				(this is for creating the bootloader binary)
2. pre-compiled Linux kernel binary   	(this is Linux itself-- the heart of the OS)
3. a Root File System for Lune OS			(this is the remainder of the OS)
4. some firmware
5. the Multiboot-Image source code		(this uses 1. 2. and 3. and puts them together to create the 2-partition disk image we need to boot the phone.


##### 1. Make a Directory to hold all our work
I called mine 'LuneImgBuild'.  Get inside it.
`mkdir LuneImgBuild && cd LuneImgBuild`



##### 2. Create a local clone of the P-Boot repo (contains bootloader source code):
Megi's P-Boot bootloader is hosted at https://megous.com/git/p-boot/
Read the about page: https://megous.com/git/p-boot/about/ and read about P-Boot, how it works, and how to build it.

> [5 pts] What byte does the PinePhone's CPU look at for bootloader bytecode??answer:0x83

It is the EABI (embedded application binary interface) that specifies how executable bytecode should be organized into a file. If the AllWinner A64 does not find a file that conforms to the EABI, it will not start executing it.

Go to the summary page, and look for a link at the bottom of the summary page titled "Clone"
Copy that url.

Clone the P-boot src into a local build directory:
`cd <your-build-dir> && git clone https://megous.com/git/p-boot`

##### 3. Create a local clone of the Multi-Boot repo (contains build scripts):
Megi's Multi-Boot-Image building scripts are hosted at https://megous.com/git/pinephone-multi-boot/
Go there, and look for a link at the bottom of the summary page titled "Clone"
Copy that url.

In linux, create a local clone of Megi's repository with `git clone <paste-url>`

##### 4. Create a local clone of the Pinephone Firmware repo:
Megi has some firmware for the PinePhone hosted at https://megous.com/git/linux-firmware
Go there, and look for a link at the bottom of the summary page titled "Clone"
Copy that url.

In linux, create a local clone of Megi's repository with `git clone <paste-url>`

##### 5. Explore the Official Linux kernel

Many groups/individuals build their own kernels to suit their use-case and their hardware.  Linus Torvalds maintains the source code for the "generic" Linux Kernel himself... still. Generic Linux refers to the most "upstream" source code (from the source) which are the versions maintained by Linus.  

Visit Linus' "Official" kernel site https://www.kernel.org and see on the front page which versions of the Kernel have which "status."  Read the FAQ's to learn about what the statuses mean.

>[5 pts] What is version the most recent kernel release? answer:mainline is 5.11

>[5 pts] What version is the latest "stable" kernel release?answer:stable is 5.11.1


##### 6. Explore Pine64's Linux kernel


Pine64, the group that designed and manufactured our phones, maintains their own version of the Linux Kernel.  Their source code is hosted at https://gitlab.com/pine64-org/linux .
Visit that page and see what branches of their project are active.
(go to "Repository - > Branches" in the left nav. pane)

> [5 pts] What is the name of the most active branch? answer:pine64-kernel-ubports-5.10.y-megi
>
> [3 pts] What version of the Linux kernel do you think that branch is based on? answer:5.10
>
> [3 pts] What is the most recent commit made to that branch? answer:"Add ci config"
>
> [3 pts] Who authored that commit? answer:Marius Gripsgard
>
> [3 pts] When did they author the commit? answer:2 months ago
>
> [3 pts] Who authored the commit with the hash `9d640944`? answer:Ondrej Jirman
> [5 pts] Does that person go by another name? answer: I do not think he does.


##### 7. Explore Megi's Linux kernel

Megi also maintains his own versions of the Linux kernel.  Visit his page at 
https://megous.com/git/linux/.

> [3pts] Which branch of his repo do you think he uses for the PinePhone? answer:orange-pi
> [3pts] What version of the kernel is that based on? answer:5.12
> [3pts] When was Megi's latest commit to the pp-5.11 branch? answer:arm64: dts: sun50i-pinephone: Add interrupt pin for WiFi
> [3pts] What file was modified in that commit? answer:WiFi


##### 8. Download a Pre-Built copy of Megi's Linux kernel

Megi compiles his kernel code and provides the executable images for download at:
https://xff.cz/kernels/.
Go there and see which versions of his kernel are available.

Read his `README.md` file to see where he publishes his public key.  We'll need this to
verify the integrity and authenticity of his kernel image.

Download the most recent `pp.tar.gz` and `pp.tar.gz.asc` files into a new directory called 'kernels'

> [3 pts] Fetch Megi's key from the terminal using `curl <paste-link-here>` (screenshot)
Notice that `curl` reads content returned by accessing an internet url and prints the content
to `stdout` (which by default is your terminal)

>[3 pts] Try `curl www.google.com` (screenshot) and see what comes back.  What kind of file is that?

>[3 pts] This time, fetch megi's key but redirect stdout to a file (save in repo) called 'megi.key':  `curl <url-here>   >  megi.key`

This time, fetch the key, and redirect it directly into your GPG keyring:
`curl <paste-link-here> | gpg --import -`

The vertical line in the last command is called a "pipe."  It is used to pipe the output of the first command (stdout) into the next command's stdin.  Just like plumbing.  The '-' tells
gpg to import from stdin.

>[3 pts] Now you can verify the file: `gpg --verify pp.tar.gz.asc pp.tar.gz`(screenshot) be sure you see "Good signature from Ondrej Jirman...."

Extract the tarball that we just verified: `tar -xf <filename-goes-here>`.
You can now delete the .gz and .gz.asc files if you like.

##### 9. Setup a build environment to make LuneOS:

https://www.webos-ports.org/wiki/Build_for_Pinephone details how the LuneOS image is built for the pinephone.

We are going to follow the directions there (maybe with a few modifications).

1) All POSIX-compliant OSes provide a program located at `/bin/sh` which behaves as a "shell."
A shell is a basic command line input/output system.  By default, in Ubuntu, /bin/sh just points to `/bin/dash` which is a very very simple and inflexible shell. All the Lune build scripts are written for bash, even though some are invoked with `/bin/sh`.  Therefore, we want `/bin/sh` to point to `/bin/bash` instead:
```
sudo dpkg-reconfigure dash
```

2) There is a typo in the webOS instructions. We want version 14, not 4:

```
$ curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
```
Then, as the output of the previous command says, install nodejs: 
```
sudo apt install -y nodejs
```

3) Use `apt` to install all the other tools we'll need:

```
gawk wget git-core diffstat unzip texinfo build-essential chrpath libsdl1.2-dev xterm bzr
```

4) Setup a build-environment for LuneOS (a webOS port):

```bash
cd into/your/build-directory
mkdir webos-ports-env && cd webos-ports-env
```

5) Create a variable in the shell; it will be exported into the environments of all child processes:

```
export ENV_NAME=testing
```
The value of the variable can be dereferenced by using `$` and optionally enclosing the name in braces: `${ENV_NAME}`.

> [3pts ] Print the value of the variable to the terminal with the `echo` command:
`echo ${ENV_NAME}` (screenshot: echo.png)


6) Get the makefile from the webos github server:

```
wget https://raw.github.com/webOS-ports/webos-ports-setup/$ENV_NAME/Makefile
```

7) Run the 'setup-webos-ports' section of the makefile with:

```
make setup-webos-ports
```

##### 10. Build LuneOS:

1) make sure you're in the `webos-ports-env` directory

2) Source the 'setup-env' file found at `webos-ports/setup-env` by running the command `source webos-ports/setup-env`  (note that the webOS-ports webpage uses the `.` command instead of `source` -- these are identical commands.  `.` is just an alias for `source`).

When you 'source' a file, the shell will read and execute the contents of that file line-by-line just as though they were typed into the shell on the command line.

After sourcing the file, you should see your prompt change from something like `user@machine-name:` to `OE @luneos`

> [10pts] Take a screenshot of your old and new bash prompts (prompts.png)

3) run the 'update' portion of the makefile: `make update`

4) cd back into the 'webos-ports' directory

5) simultaneously set the environment variable 'MACHINE' to 'pinephone' immediately before calling `bb` to start the build:

```
MACHINE=pinephone bb luneos-dev-package
```

6) Sit back and watch the fireworks... for a long time.  If you need to stop the build, press <ctl>+c and wait for BB to clean up.  You can resume the build by repeating steps 1)-5) above.

> [10pts] Wait for BitBake to start "executing tasks..." and then take a screenshot of BitBake running (bitbake.png) 
